#import "RootViewController.h"
#import "GridTileOverlay.h"

@import MapKit;

@interface RootViewController () <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) MKTileOverlay *tileOverlay;
@property (strong, nonatomic) MKTileOverlay *gridOverlay;
@property (strong, nonatomic) MKAnnotationView *pinAnnotationView;

@end

@implementation RootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // load grid tile overlay for debugging
    
    //self.gridOverlay = [[GridTileOverlay alloc] init];
    //self.gridOverlay.canReplaceMapContent=NO;
    //[self.mapView addOverlay:self.gridOverlay level:MKOverlayLevelAboveLabels];

    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = CLLocationCoordinate2DMake(50.7218884, 7.0612619);
    
    //loading pois
    self.pinAnnotationView = [[MKAnnotationView alloc] initWithAnnotation:point
                                                            reuseIdentifier:@"Shopper"];
    self.pinAnnotationView.image = [UIImage imageNamed:@"shopper.png"];
    
    [self.mapView addAnnotation:point];

    [self reloadTileOverlay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)reloadTileOverlay {
    if(self.tileOverlay) {
        [self.mapView removeOverlay:self.tileOverlay];
    }

    NSString *urlTemplate = nil;
    urlTemplate = @"http://87.230.17.161:80/maps/tarentAnbau2/tiles/{z}/{x}/{y}.jpg";
    self.tileOverlay = [[MKTileOverlay alloc] initWithURLTemplate:urlTemplate];
    
    //flip coordinates because gdal exports with origin (0, 0) leftBottom
    self.tileOverlay.geometryFlipped = true;
    
    self.tileOverlay.canReplaceMapContent=YES;
    [self.mapView insertOverlay:self.tileOverlay belowOverlay:self.gridOverlay];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id <MKAnnotation>)annotation
{
    // If the annotation is the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]]){
        return nil;
    } else {
        return self.pinAnnotationView;
    }
    
    return nil;
}


-(MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    if([overlay isKindOfClass:[MKTileOverlay class]]) {
        MKTileOverlay *tileOverlay = (MKTileOverlay *)overlay;
        MKTileOverlayRenderer *renderer = nil;
        
        renderer = [[MKTileOverlayRenderer alloc] initWithTileOverlay:tileOverlay];

        return renderer;
    }
    
    return nil;
}

@end
