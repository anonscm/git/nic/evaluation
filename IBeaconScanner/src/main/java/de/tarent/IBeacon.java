package de.tarent;

import java.nio.ByteBuffer;
import java.util.UUID;

public class IBeacon {
    private static final byte[] APPLE_IDENTIFIER = {0x4C, 0x00};
    private static final byte[] IBEACON_SERVICE_IDENTIFIER = {0x02, 0x15};

    private UUID proximityUUID;
    private short major;
    private short minor;
    private byte txPower;

    public IBeacon() {

    }

    public UUID getProximityUUID() {
        return proximityUUID;
    }

    public void setProximityUUID(UUID proximityUUID) {
        this.proximityUUID = proximityUUID;
    }

    public short getMajor() {
        return major;
    }

    public void setMajor(short major) {
        this.major = major;
    }

    public short getMinor() {
        return minor;
    }

    public void setMinor(short minor) {
        this.minor = minor;
    }

    public byte getTxPower() {
        return txPower;
    }

    public void setTxPower(byte txPower) {
        this.txPower = txPower;
    }

    public static IBeacon fromAdvertisementData(byte[] advertisementData) {
        ByteBuffer bb = ByteBuffer.wrap(advertisementData);

        while (bb.hasRemaining()) {
            if (bb.get() == APPLE_IDENTIFIER[0]
                    && bb.get() == APPLE_IDENTIFIER[1]
                    && bb.get() == IBEACON_SERVICE_IDENTIFIER[0]
                    && bb.get() == IBEACON_SERVICE_IDENTIFIER[1]) {

                //check if the remaining beacon can possibly hold a proximityUUID, 2 shorts and 1 byte
                if (bb.remaining() < 21) {
                    return null;
                }

                IBeacon beacon = new IBeacon();
                beacon.proximityUUID = new UUID(bb.getLong(), bb.getLong());
                beacon.major = bb.getShort();
                beacon.minor = bb.getShort();
                beacon.txPower = bb.get();

                return beacon;
            }
        }

        return null;
    }
}
