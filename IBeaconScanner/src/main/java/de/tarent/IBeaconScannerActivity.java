package de.tarent;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;

public class IBeaconScannerActivity extends Activity {
    private BluetoothAdapter bluetoothAdapter;
    private Handler handler;
    private ScanResultAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler();

        ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new ScanResultAdapter();
        listView.setAdapter(adapter);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.clearDevices();
            }
        });

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkBluetoothLESupported()) {
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothAdapter = bluetoothManager.getAdapter();

            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }

            startScan();
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi,
                                     final byte[] scanRecord) {
                    Log.d("BTLEScanner", "BTLEScanResult: " + device.getAddress() + " | " + rssi);

                    final BeaconInfo btleDevice = new BeaconInfo(device.getName(), device.getAddress(), rssi, scanRecord);
                    if(btleDevice.beacon != null){
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                 adapter.updateDevice(btleDevice);
                            }
                        });
                    }
                }
            };

    private void startScan() {
        Log.d("BTLEScanner", "Starting BTLE Scan");
        if (!bluetoothAdapter.startLeScan(mLeScanCallback)) {
            Log.d("BTLEScanner", "Error starting BTLE Scan");
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopLeScan();
                startScan();
            }
        }, 1000);
    }

    private void stopLeScan() {
        Log.d("BTLEScanner", "Stopping BTLE Scan");
        bluetoothAdapter.stopLeScan(mLeScanCallback);
    }

    public boolean checkBluetoothLESupported() {
        boolean supported = getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);

        if (!supported) {
            Toast.makeText(this, "BTLE not supported", Toast.LENGTH_LONG).show();
        }

        return supported;
    }

    public class ScanResultAdapter extends BaseAdapter {
        private ArrayList<BeaconInfo> devices = new ArrayList<BeaconInfo>();

        public ScanResultAdapter() {

        }

        @Override
        public int getCount() {
            return devices.size();
        }

        @Override
        public Object getItem(int position) {
            return devices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void updateDevice(BeaconInfo btleDevice) {
            boolean updated = false;
            for (int i = 0; i < devices.size(); i++) {
                if (devices.get(i).address.equals(btleDevice.address)) {
                    devices.set(i, btleDevice);
                    updated = true;
                    break;
                }
            }

            if (!updated) {
                devices.add(btleDevice);
            }

            if(btleDevice.rssi > 0){
                devices.remove(btleDevice);
            }

            this.notifyDataSetChanged();
        }

        public void clearDevices() {
            devices.clear();
            this.notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.listelem, parent, false);
            }

            TextView tvDeviceId = (TextView) v.findViewById(R.id.deviceId);
            TextView tvMajor = (TextView) v.findViewById(R.id.major);
            TextView tvMinor = (TextView) v.findViewById(R.id.minor);
            TextView tvProximityUUID = (TextView) v.findViewById(R.id.proximityUUID);
            TextView tvRssi = (TextView) v.findViewById(R.id.rssi);

            BeaconInfo btleDevice = devices.get(position);
            tvDeviceId.setText(btleDevice.address);
            tvMajor.setText(String.valueOf(btleDevice.beacon.getMajor()));
            tvMinor.setText(String.valueOf(btleDevice.beacon.getMinor()));
            tvProximityUUID.setText(btleDevice.beacon.getProximityUUID().toString());
            tvRssi.setText(String.valueOf(btleDevice.rssi));

            return v;
        }
    }

    private class BeaconInfo {
        String address;
        int rssi;
        String name;
        IBeacon beacon;

        public BeaconInfo(String name, String addr, int rssi, byte[] scanRecord) {
            this.address = addr;
            this.rssi = rssi;
            this.name = name;
            this.beacon = IBeacon.fromAdvertisementData(scanRecord);
        }
    }
}


