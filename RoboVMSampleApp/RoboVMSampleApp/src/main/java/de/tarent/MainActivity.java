package de.tarent;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TextView t = (TextView)findViewById(R.id.textView);
        t.setText("The answer the the heavy calculation is: " + RoboVMSampleLibrary.heavyCalculation());
    }
}

