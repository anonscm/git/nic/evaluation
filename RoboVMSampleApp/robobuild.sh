#!/bin/bash

robo="robovm/bin/robovm"
arch="x86"
os="ios"
robovm_classpath="robovm/lib/robovm-objc.jar:robovm/lib/robovm-cocoatouch.jar"
app_classpath="RoboVMSampleApp-ios/target/RoboVMSampleApp-ios-1.0-SNAPSHOT.jar:RoboVMSampleLibrary/target/RoboVMSampleLibrary-1.0-SNAPSHOT.jar"
mainclass=de.tarent.RoboVMSampleIOSApp

command="${robo} -verbose -arch ${arch} -os ${os} -cp ${robovm_classpath}:${app_classpath} -run ${mainclass}"

$command