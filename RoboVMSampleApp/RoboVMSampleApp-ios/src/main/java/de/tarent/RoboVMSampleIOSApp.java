package de.tarent;

import org.robovm.apple.coregraphics.*;
import org.robovm.apple.foundation.*;
import org.robovm.apple.uikit.*;

public class RoboVMSampleIOSApp extends UIApplicationDelegateAdapter {

    private UIWindow window = null;
    private int clickCount = 0;

    @Override
    public boolean didFinishLaunching(UIApplication application,
            NSDictionary<NSString, ?> launchOptions) {


        final UITextField textField = new UITextField(UIScreen.getMainScreen().getBounds());
        textField.setText("Result: " + RoboVMSampleLibrary.heavyCalculation());

        window = new UIWindow(UIScreen.getMainScreen().getBounds());
        window.setBackgroundColor(UIColor.colorLightGray());
        window.addSubview(textField);
        window.makeKeyAndVisible();

        return true;
    }

    public static void main(String[] args) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(args, null, RoboVMSampleIOSApp.class);
        pool.close();
    }
}