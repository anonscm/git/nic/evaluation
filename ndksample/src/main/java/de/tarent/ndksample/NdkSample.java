package de.tarent.ndksample;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

public class NdkSample extends Activity {
    //we need to load the library (libndksample.so) at runtime
    //libndksample.so -> ndksample and not libndksample
    static {
        System.loadLibrary("gnustl_shared");
        System.loadLibrary("ndksample");
    }

    //native function declaration
    //calling this causes the runtime to search for a function named
    //jstring Java_de_tarent_ndksample_NdkSample_hello( JNIEnv* env, jobject thiz )
    //in native code
    public native String hello();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView text = (TextView)findViewById(R.id.textView);
        text.setText(hello());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}

