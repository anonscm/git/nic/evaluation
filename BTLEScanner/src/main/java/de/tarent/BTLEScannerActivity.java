package de.tarent;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class BTLEScannerActivity extends Activity {
    private BluetoothAdapter bluetoothAdapter;
    private Handler handler;
    private BTLEAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        handler = new Handler();

        ListView listView = (ListView) findViewById(R.id.listView);
        adapter = new BTLEAdapter();
        listView.setAdapter(adapter);

        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.clearDevices();
            }
        });

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkBluetoothLESupported()) {
            final BluetoothManager bluetoothManager =
                    (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            bluetoothAdapter = bluetoothManager.getAdapter();

            if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, 1);
            }

            startScan();
        }
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi,
                                     final byte[] scanRecord) {
                    Log.d("BTLEScanner", "BTLEScanResult: " + device.getAddress() + " | " + rssi);

                    final BTLEDevice btleDevice = new BTLEDevice(device, rssi);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.updateDevice(btleDevice);
                        }
                    });
                }
            };

    private void startScan() {
        Log.d("BTLEScanner", "Starting BTLE Scan");
        if (!bluetoothAdapter.startLeScan(mLeScanCallback)) {
            Log.d("BTLEScanner", "Error starting BTLE Scan");
        }

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopLeScan();
                startScan();
            }
        }, 1000);
    }

    private void stopLeScan() {
        Log.d("BTLEScanner", "Stopping BTLE Scan");
        bluetoothAdapter.stopLeScan(mLeScanCallback);
    }

    public boolean checkBluetoothLESupported() {
        boolean supported = getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);

        if (!supported) {
            Toast.makeText(this, "BTLE not supported", Toast.LENGTH_LONG).show();
        }

        return supported;
    }

    public class BTLEAdapter extends BaseAdapter {
        private ArrayList<BTLEDevice> devices = new ArrayList<BTLEDevice>();

        public BTLEAdapter() {

        }

        @Override
        public int getCount() {
            return devices.size();
        }

        @Override
        public Object getItem(int position) {
            return devices.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        public void updateDevice(BTLEDevice btleDevice) {
            boolean updated = false;
            for (int i = 0; i < devices.size(); i++) {
                if (devices.get(i).address.equals(btleDevice.address)) {
                    devices.set(i, btleDevice);
                    updated = true;
                    break;
                }
            }

            if (!updated) {
                devices.add(btleDevice);
            }

            this.notifyDataSetChanged();
        }

        public void clearDevices() {
            devices.clear();
            this.notifyDataSetChanged();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.listelem, parent, false);
            }

            TextView tvDeviceId = (TextView) v.findViewById(R.id.deviceId);
            TextView tvRssi = (TextView) v.findViewById(R.id.rssi);
            TextView tvTimestamp = (TextView) v.findViewById(R.id.timestamp);
            TextView tvName = (TextView) v.findViewById(R.id.name);

            BTLEDevice btleDevice = devices.get(position);
            tvDeviceId.setText(btleDevice.address);
            tvRssi.setText(String.valueOf(btleDevice.rssi));
            tvTimestamp.setText(btleDevice.timestamp);

            tvName.setText(btleDevice.name);
            return v;
        }
    }

    private class BTLEDevice {
        String address;
        int rssi;
        String timestamp;
        String name;

        public BTLEDevice(BluetoothDevice device, int rssi) {
            this.address = device.getAddress();
            this.rssi = rssi;
            this.name = device.getName();

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            timestamp = dateFormat.format(new Date());
        }
    }
}


