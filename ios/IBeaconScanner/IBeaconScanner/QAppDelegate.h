//
//  QAppDelegate.h
//  BTLE Scanner
//
//  Created by Ronny Pflug on 04/06/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
