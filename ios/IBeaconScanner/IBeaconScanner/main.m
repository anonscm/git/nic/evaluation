//
//  main.m
//  BTLE Scanner
//
//  Created by Ronny Pflug on 04/06/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "QAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([QAppDelegate class]));
    }
}
