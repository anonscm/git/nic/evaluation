//
//  QBlueToothTableViewController.h
//  BTLE Scanner
//
//  Created by Ronny Pflug on 05/06/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QBlueToothTableViewController : UITableViewController

@end
