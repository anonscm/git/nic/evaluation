//
//  QBlueToothTableViewController.m
//  BTLE Scanner
//
//  Created by Ronny Pflug on 05/06/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import "QBlueToothTableViewController.h"
#import "BLEDeviceManager.h"
#import "BLEFingerprintManager.h"

@interface QBlueToothTableViewController ()

@property (strong, nonatomic) BLEDeviceManager *deviceManager;

@end

@implementation QBlueToothTableViewController {
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.deviceManager = [BLEDeviceManager defaultManager];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.deviceManager startMonitoringWithRegion:[[NSUUID alloc] initWithUUIDString:@"bec26202-a8d8-4a94-b0fc-9ac1de37daa6"]];
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(invalidateTable) userInfo:nil repeats:true];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
      
- (void)invalidateTable
{
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.deviceManager.allBeacons count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QBlueToothDevicePeripheralCellId" forIndexPath:indexPath];
    
    CLBeacon *beacon = [self.deviceManager.allBeacons objectAtIndex:indexPath.row];
   
    UILabel *labelRssi = (UILabel *)[cell viewWithTag:100];
    labelRssi.text = [NSString stringWithFormat:@"%i", beacon.rssi];
   
    UILabel *labelMajor = (UILabel *)[cell viewWithTag:200];
    labelMajor.text = [NSString stringWithFormat:@"%hi", beacon.major.shortValue];
    
    UILabel *labelMinor = (UILabel *)[cell viewWithTag:300];
    labelMinor.text = [NSString stringWithFormat:@"%hi", beacon.minor.shortValue];
    
    return cell;
}

@end
