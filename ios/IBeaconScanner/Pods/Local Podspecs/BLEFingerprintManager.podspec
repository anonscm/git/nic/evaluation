Pod::Spec.new do |s|
  s.name     = 'BLEFingerprintManager'
  s.version  = '0.1.0'
  s.authors  = { 'Ronny Pflug' =>
                 'ronny.pflug@cannysoft.de' }
  s.source_files = 'BLE Fingerprint Manager'
  s.requires_arc = true
end
