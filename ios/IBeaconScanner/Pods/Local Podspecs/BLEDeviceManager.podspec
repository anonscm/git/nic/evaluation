Pod::Spec.new do |s|
  s.name     = 'BLEDeviceManager'
  s.version  = '0.2.1'
  s.authors  = { 'Ronny Pflug' =>
                 'ronny.pflug@cannysoft.de' }
  s.source_files = 'BLE Device Manager'
  s.requires_arc = true
end
