//
//  BLEFingerprintManager.m
//  BLE Fingerprint Manager
//
//  Created by Ronny Pflug on 17/07/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import "BLEFingerprintManager.h"

const NSString *kFingerprintsFilename = @"fingerprints_data.json";

@implementation BLEFingerprintManager

@synthesize fingerprints = _fingerprints;

- (id)init
{
    self = [super init];
    if (self) {
        _fingerprints = [[NSArray alloc] init];
    }
    return self;
}

+ (BLEFingerprintManager *)defaultManager
{
    static BLEFingerprintManager *defaultManager = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        defaultManager = [[BLEFingerprintManager alloc] init];
    });
    return defaultManager;
}

- (void)addFingerprint:(BLEFingerprint *)fingerprint
{
    // TODO: Check for duplicates
    NSMutableArray *temporaryFingerprints = [_fingerprints mutableCopy];
    [temporaryFingerprints addObject:fingerprint];
    
    _fingerprints = temporaryFingerprints;
}

- (void)saveFingerprintsToDisk
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // saving all fps
    NSString *fileName = [NSString stringWithFormat:@"%@/%@", documentsDirectory, kFingerprintsFilename];
    NSMutableArray *fingerprints = [[NSMutableArray alloc] init];
    
    for (BLEFingerprint *fp in self.fingerprints) {
        [fingerprints addObject:[fp dictionaryRepresentation]];
    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:fingerprints options:NSJSONWritingPrettyPrinted error:nil];
    NSError *error = nil;
    
    [jsonData writeToFile:fileName options:NSDataWritingAtomic error:&error];
    
    if (error) {
        NSLog(@"error: %@", error);
    } else {
        NSLog(@"saved fingerprints to file: %@", fileName);
    }
}

@end
