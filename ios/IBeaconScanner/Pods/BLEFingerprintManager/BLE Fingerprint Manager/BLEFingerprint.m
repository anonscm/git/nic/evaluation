//
//  BLEFingerprint.m
//  BLE Fingerprint Manager
//
//  Created by Ronny Pflug on 17/07/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import "BLEFingerprint.h"

#import "BLEHistogram.h"
#import "BLEPoint.h"

NSString *const kBLEFingerprintId = @"id";
NSString *const kBLEFingerprintHistogram = @"histogram";
NSString *const kBLEFingerprintPoint = @"point";

@interface BLEFingerprint ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BLEFingerprint

@synthesize identifier = _identifier;
@synthesize histogram = _histogram;
@synthesize point = _point;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.identifier = [self objectOrNilForKey:kBLEFingerprintId fromDictionary:dict];
        self.histogram = [BLEHistogram modelObjectWithDictionary:[dict objectForKey:kBLEFingerprintHistogram]];
        self.point = [BLEPoint modelObjectWithDictionary:[dict objectForKey:kBLEFingerprintPoint]];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.identifier forKey:kBLEFingerprintId];
    [mutableDict setValue:[self.histogram dictionaryRepresentation] forKey:kBLEFingerprintHistogram];
    [mutableDict setValue:[self.point dictionaryRepresentation] forKey:kBLEFingerprintPoint];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.identifier = [aDecoder decodeObjectForKey:kBLEFingerprintId];
    self.histogram = [aDecoder decodeObjectForKey:kBLEFingerprintHistogram];
    self.point = [aDecoder decodeObjectForKey:kBLEFingerprintPoint];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeObject:_identifier forKey:kBLEFingerprintId];
    [aCoder encodeObject:_histogram forKey:kBLEFingerprintHistogram];
    [aCoder encodeObject:_point forKey:kBLEFingerprintPoint];
}

- (id)copyWithZone:(NSZone *)zone
{
    BLEFingerprint *copy = [[BLEFingerprint alloc] init];
    
    if (copy) {
        
        copy.identifier = [self.identifier copyWithZone:zone];
        copy.histogram = [self.histogram copyWithZone:zone];
        copy.point = [self.point copyWithZone:zone];
    }
    
    return copy;
}

#pragma mark - Initializer
- (instancetype)initWithIdentifier:(NSString *)identifier {
    self = [super init];
    if (self) {
        _identifier = identifier;
    }
    return self;
}

- (instancetype)initWithIdentifier:(NSString *)identifier andPoint:(BLEPoint *)point {
    self = [self initWithIdentifier:identifier];
    if (self) {
        _point = point;
    }
    return self;
}

- (instancetype)initWithIdentifier:(NSString *)identifier histogram:(BLEHistogram *)histogram andPoint:(BLEPoint *)point
{
    self = [self initWithIdentifier:identifier andPoint:point];
    if (self) {
        _histogram = histogram;
    }
    return self;
}




@end
