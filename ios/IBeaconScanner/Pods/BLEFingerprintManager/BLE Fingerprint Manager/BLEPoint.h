//
//  BLEPoint.h
//  BLE Fingerprint Manager
//
//  Created by Ronny Pflug on 17/07/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLEPoint : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) double latitude;
@property (nonatomic, assign) double divergence;
@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) double altitude;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
