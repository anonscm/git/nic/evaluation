//
//  BLEFingerprint.h
//  BLE Fingerprint Manager
//
//  Created by Ronny Pflug on 17/07/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEPoint.h"
#import "BLEHistogram.h"

@interface BLEFingerprint : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) BLEHistogram *histogram;
@property (nonatomic, strong) BLEPoint *point;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

#pragma mark - Initializer
- (instancetype)initWithIdentifier:(NSString *)identifier histogram:(BLEHistogram *)histogram andPoint:(BLEPoint *)point;
- (instancetype)initWithIdentifier:(NSString *)identifier andPoint:(BLEPoint *)point;
- (instancetype)initWithIdentifier:(NSString *)identifier;

@end
