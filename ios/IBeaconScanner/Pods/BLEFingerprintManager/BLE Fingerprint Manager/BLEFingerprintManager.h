//
//  BLEFingerprintManager.h
//  BLE Fingerprint Manager
//
//  Created by Ronny Pflug on 17/07/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BLEFingerprint.h"

@interface BLEFingerprintManager : NSObject

/**
 *  Holds all available fingerprints
 */
@property (nonatomic, strong) NSArray *fingerprints;

/**
 *  Creates a shared instance of BLEFingerprintManager.
 *
 *  @return Returns the default manager as a singleton.
 */
+ (BLEFingerprintManager *)defaultManager;

/**
 *  Adds the given fingerprint.
 *
 *  @param fingerprint the fingerprint which should be added
 */
- (void)addFingerprint:(BLEFingerprint *)fingerprint;

/**
 *  Stores all fingerprints to a single file.
 */
- (void)saveFingerprintsToDisk;
@end
