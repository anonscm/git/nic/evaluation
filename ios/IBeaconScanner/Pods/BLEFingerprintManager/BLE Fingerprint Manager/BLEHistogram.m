//
//  BLEHistogram.m
//  BLE Fingerprint Manager
//
//  Created by Ronny Pflug on 17/07/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import "BLEHistogram.h"

const NSUInteger kMaxValues = 5;

@interface BLEHistogram ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BLEHistogram

@synthesize rawValues = _rawValues;
@synthesize values = _values;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        _values = dict;
        _rawValues = [self restoreRawValuesWithDictionary:dict];
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    return [NSDictionary dictionaryWithDictionary:self.values];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

/**
 *  Restores the raw values from a histogram dictionary.
 *
 *  @param dict the histogram dictionary
 *
 *  @return Returns the restored raw values as a NSDictionary.
 */
- (NSDictionary *)restoreRawValuesWithDictionary:(NSDictionary *)dict
{
    NSMutableDictionary *rawValues = [[NSMutableDictionary alloc] init];
    
    NSDictionary *keyValues = nil;
    NSMutableArray *rawValuesForKey = nil;
    
    for (NSString *key in dict) {
        rawValuesForKey = [[NSMutableArray alloc] init];
        keyValues = [dict objectForKey:key];
        
        NSNumber *relativeFrequency;
        NSInteger valueCount;
        for (NSString *valueKey in [keyValues allKeys]) {
            relativeFrequency = [keyValues objectForKey:valueKey];

            valueCount = kMaxValues * [relativeFrequency floatValue];
            for (int i=0; i<valueCount; i++) {
                [rawValuesForKey addObject:[NSNumber numberWithDouble:[valueKey doubleValue]]];
            }
        }
        [rawValues setObject:rawValuesForKey forKey:key];
        keyValues = nil;
        rawValuesForKey = nil;
    }
    
    return rawValues;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    _values = [aDecoder decodeObject];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:_values];
}

- (id)copyWithZone:(NSZone *)zone
{
    BLEHistogram *copy = [[BLEHistogram alloc] initWithDictionary:_values];
    
    return copy;
}

#pragma mark - Methods
- (void)addValue:(NSNumber *)value forKey:(NSString *)key
{
    NSMutableArray *keyValues = [[_rawValues objectForKey:key] mutableCopy];
    
    if (!keyValues) {
        keyValues = [[NSMutableArray alloc] init];
    }
    
    [keyValues addObject:value];
    
    NSMutableDictionary *tempRawValues = [_rawValues mutableCopy];
    
    if (!tempRawValues) {
        tempRawValues = [[NSMutableDictionary alloc] init];
    }
    
    [tempRawValues setObject:keyValues forKey:key];
    
    _rawValues = tempRawValues;
}

#pragma mark - Property accessories

- (NSDictionary *)values
{
    NSMutableDictionary *values = [[NSMutableDictionary alloc] init];
    NSArray *rawValues = nil;
    NSMutableDictionary *histogramValues = nil;
    NSInteger maxAmount = 0;
    
    for (NSString *key in _rawValues) {
        rawValues = [_rawValues valueForKey:key];
        NSCountedSet *countedRawValues = [[NSCountedSet alloc] initWithArray:rawValues];
        
        maxAmount = [rawValues count];
        
        histogramValues = [[NSMutableDictionary alloc] init];
        
        NSNumber *relativeFrequency = nil;
        for (NSNumber *currentValue in countedRawValues) {
            relativeFrequency = [NSNumber numberWithFloat:((float)[countedRawValues countForObject:currentValue]/(float)maxAmount)];
            [histogramValues setValue:relativeFrequency forKey:[currentValue stringValue]];
            relativeFrequency = nil;
        }
        
        [values setObject:histogramValues forKey:key];
        histogramValues = nil;
    }
    _values = values;
    
    return values;
}

@end
