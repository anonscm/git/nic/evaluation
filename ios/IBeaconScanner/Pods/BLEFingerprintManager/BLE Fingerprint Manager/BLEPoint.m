//
//  BLEPoint.m
//  BLE Fingerprint Manager
//
//  Created by Ronny Pflug on 17/07/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import "BLEPoint.h"

NSString *const kBLEPointLatitude = @"mLatitudeE6";
NSString *const kBLEPointDivergence = @"divergence";
NSString *const kBLEPointLongitude = @"mLongitudeE6";
NSString *const kBLEPointAltitude = @"mAltitude";

@interface BLEPoint ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end


@implementation BLEPoint

@synthesize latitude = _latitude;
@synthesize divergence = _divergence;
@synthesize longitude = _longitude;
@synthesize altitude = _altitude;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.latitude = [[self objectOrNilForKey:kBLEPointLatitude fromDictionary:dict] doubleValue];
        self.divergence = [[self objectOrNilForKey:kBLEPointDivergence fromDictionary:dict] doubleValue];
        self.longitude = [[self objectOrNilForKey:kBLEPointLongitude fromDictionary:dict] doubleValue];
        self.altitude = [[self objectOrNilForKey:kBLEPointAltitude fromDictionary:dict] doubleValue];
        
    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:[NSNumber numberWithDouble:self.latitude] forKey:kBLEPointLatitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.divergence] forKey:kBLEPointDivergence];
    [mutableDict setValue:[NSNumber numberWithDouble:self.longitude] forKey:kBLEPointLongitude];
    [mutableDict setValue:[NSNumber numberWithDouble:self.altitude] forKey:kBLEPointAltitude];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.latitude = [aDecoder decodeDoubleForKey:kBLEPointLatitude];
    self.divergence = [aDecoder decodeDoubleForKey:kBLEPointDivergence];
    self.longitude = [aDecoder decodeDoubleForKey:kBLEPointLongitude];
    self.altitude = [aDecoder decodeDoubleForKey:kBLEPointAltitude];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    [aCoder encodeDouble:_latitude forKey:kBLEPointLatitude];
    [aCoder encodeDouble:_divergence forKey:kBLEPointDivergence];
    [aCoder encodeDouble:_longitude forKey:kBLEPointLongitude];
    [aCoder encodeDouble:_altitude forKey:kBLEPointAltitude];
}

- (id)copyWithZone:(NSZone *)zone
{
    BLEPoint *copy = [[BLEPoint alloc] init];
    
    if (copy) {
        
        copy.latitude = self.latitude;
        copy.divergence = self.divergence;
        copy.longitude = self.longitude;
        copy.altitude = self.altitude;
    }
    
    return copy;
}


@end
