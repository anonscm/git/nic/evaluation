//
//  BLEHistogram.h
//  BLE Fingerprint Manager
//
//  Created by Ronny Pflug on 17/07/14.
//  Copyright (c) 2014 Ronny Pflug. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BLEHistogram : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong, readonly) NSDictionary *values;
@property (nonatomic, strong, readonly) NSDictionary *rawValues;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

#pragma mark - Methods
- (void)addValue:(NSNumber *)value forKey:(NSString *)key;

@end
