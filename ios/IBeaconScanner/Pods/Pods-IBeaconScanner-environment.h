
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// BLEDeviceManager
#define COCOAPODS_POD_AVAILABLE_BLEDeviceManager
#define COCOAPODS_VERSION_MAJOR_BLEDeviceManager 0
#define COCOAPODS_VERSION_MINOR_BLEDeviceManager 2
#define COCOAPODS_VERSION_PATCH_BLEDeviceManager 1

// BLEFingerprintManager
#define COCOAPODS_POD_AVAILABLE_BLEFingerprintManager
#define COCOAPODS_VERSION_MAJOR_BLEFingerprintManager 0
#define COCOAPODS_VERSION_MINOR_BLEFingerprintManager 1
#define COCOAPODS_VERSION_PATCH_BLEFingerprintManager 0

